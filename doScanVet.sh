#!/bin/bash

minJob=5
mxJob=14
exec=vet
jobTime_min=8

echo scan MOps perfomance using VET $minJob to  maxJobs=$mxJob each $jobTime_min min

params0=" --runTime $jobTime_min --readFile none --writeFile -1 "
echo "External loop staging complexity of test "
for (( k=$minJob; k <= $mxJob; k++ )) ; do
    nJob=$k
    if [ $k -eq 7 ]; then nJob=8 ; fi  
    if [ $k -eq 8 ]; then nJob=10 ; fi  
    if [ $k -eq 9 ]; then nJob=12 ; fi  
    if [ $k -eq 10 ]; then nJob=14 ; fi  
    if [ $k -eq 11 ]; then nJob=16 ; fi  
    if [ $k -eq 12 ]; then nJob=20 ; fi  
    if [ $k -eq 13 ]; then nJob=24 ; fi  
    if [ $k -eq 14 ]; then nJob=28 ; fi  
    core0=vet-stage$nJob
    echo "---------stage $nJob of $mxJob , core0=$core0 "
    w |head -1
    date
    for (( i=1; i <= $nJob; i++ )) ; do
        core=$core0\_$i
	params="$params0 --coreName $core "
	echo  "loop(k=$k,i=$i) for  ./$exec $params "
	delSec=1 
	(time ./$exec $params ) >& /tmp/log.$core &
	sleep $delSec
    done
    echo done with launching this batch of jobs:
    pgrep $exec |nl
    echo "master script sleeps for $jobTime_min +1 minutes ...."
    for (( i=0; i <= $jobTime_min; i++ )) ; do
	sleep 60
	echo " $i minute ..."
    done
    echo "...... wake-up, in 10 seconds check if all jobs finished:"
    sleep 10
    pgrep $exec |nl
    grep \#vet-h /tmp/log.$core0\_1
    grep \#vet-d /tmp/log.$core0\_*
    echo after check, kill any survivors
    pkill $exec
    sleep 5
    echo check2 if no jobs again 
    pgrep $exec |nl
    echo after check2

done

echo active jobs of this type

exit
echo to kill them all exec:  pkill $exec
echo to clean output exec:   rm -f "/tmp/vet-jan_*"
echo used c++ compiler is
c++ --version|head -1


exit
# echo "Random number $i: $RANDOM"
