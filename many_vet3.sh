#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

numBfc=$1

startSkew=10 # (seconds), random delay for task
n=0
for k  in `seq 2 ${numBfc}` ; do
    # add random spread of job starts
    nsleep=$(($RANDOM % $startSkew))
    echo skew nsleep=$nsleep next fire root4star $k
    sleep $nsleep
    echo  fire root4star  k=$k
    ~/vm-eval-tool/vet3.exe  --coreName aa$k --virtRAM 3000   --useRAM 4000 --runTime 240 >& /tmp/balewski/vet-aa$k &

    n=$[ $n +1]

done

echo fired $n in the background
echo " DONE-oneNodeStar host-"`hostname`
date
