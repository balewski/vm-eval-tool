// $Id: $
// Descripion: VMevalTool ver3 , allows for stress-test of VM
// Author: Jan Balewski  , 2014-01
//
//  to compile:  c++ vet_v3.cxx -o vet3.exe
//
// To run for fixed amount of time
// time ./vet3.exe --runTime 10 --readEventSize 2000 --writeEventSize 10 --computeEventTime 20 --inputName /tmp/vet.data.inp
//
// To produce 21 GB output file in ~2 minutes, uses 2GB RAM
// time ./vet3.exe --useRAM 2000  --writeEventSize 700 --setEventTime 2 --runTime 1 --allowBigWrite --coreName=bigData --workPath /project/projectdirs/mpccc/balewski/out1
//
// To run producing fixed amount of computation and output
// time ./vet3.exe --useRAM 1100 --setNumEvent 20 --setEventWork 20 --writeEventSize 3 --coreName=out77.dat  --workPath=./out/
//
//  To create a 2GB megabyte file, takes 14 seconds, write sepped  148 MB/s
//  time   dd if=/dev/zero of=/tmp/vet.data.inp bs=1M count=2000
//
//  Logic:  event : track 
//    adjustement:  track=fixed # of ramIO+Math, about 1 CPU second on Dell-1950
//                  event( nTrack) ==> requested event time
//                  job (nEvents) ==> requested job time
//
//
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <math.h>
#include <ctime>
using namespace std;

const int MA=4; //number of arrays to be multiplexed
double *bigArrayM[MA];
//double *bigArray;
int sizeN=0;
double writeTotMB=0, readTotMB=0;
FILE *fpOut=0, *fpInp=0;
long int seed = 123456789;
int parHardWriteMax_MB=4000;
int writeEventSize_kB = 0;
int readEventSize_kB = 0;
int allowBigWrite  = false;
int leakRAM_MB  = 0;

//=========================================
int jan_rand(){ // my primitive random generator
  const long int a = 1103515245, c = 12345;
  seed = (a * seed + c) % RAND_MAX;
  return seed;
}

//=========================================
void initRamArray(int sizeMB){
  int sizeB=sizeMB*1024*1024;
  assert(sizeB>0);
  sizeN=sizeB/sizeof(double)-10; // just in case do not use all
  //bigArray= (double*)malloc(sizeB);
  for (int m=0; m<MA; m++) {
    bigArrayM[m]= (double*)malloc(sizeB);
    if(bigArrayM[m]==NULL) { printf("initRamArray malloc(%d) failed, abort\n",sizeB); exit(1);}
  }

  printf("malloc bigArray/MB=%d tot sizeN=%.2e Bytes \n",sizeMB*MA,8.*MA*sizeN);
  
}



//=========================================
int oneTrack(){ // elapsed CPU(sec)=0.1 on Dell-1950
  const int nL=100;
  const int nOps=2200;
  int totIter=0;
  //................ compute & RAM access ..........
  for(int k=0;k<nL;k++) { 
    for(int i=0;i<nOps;i++) { // goal: 1ms per internal loop
      int j=jan_rand()%sizeN;
      double x=jan_rand()*1./RAND_MAX;
      double y=jan_rand()*1./RAND_MAX;
      double z=sin(x)*pow(y,1.123);
      int m=j%MA; // pick array at random 
      bigArrayM[m][j]=z;
      int j2=jan_rand()%sizeN;
      bigArrayM[m][j2]=pow( bigArrayM[m][j], 2.3 );
      totIter++;
    }// end-msec-loop
  }
  return  totIter;
}

//=========================================
double oneEvent(int eveId, int nTrack){ 
  assert(sizeN>1);
  clock_t beginCpuT = clock(); // CPU time in usec
  time_t beginWallT; time(& beginWallT); // 1 second accuracy
  int totIter=0;

  if(fpInp) { //................ input FILE
    int  readBlock = readEventSize_kB*1024;
    readTotMB+=readEventSize_kB/1024.;
    int j=random()%sizeN/3;
    int ret=fread(bigArrayM[0]+j, 1, readBlock, fpInp);
    //printf("xx %f %d  ret=%d\n", readTotMB,readBlock,ret);
    if(ret!= readBlock ) {
      printf("end of input file, ret=%d, closed\n",ret);
      fclose(fpInp);
      fpInp=0;
    }
  }
  
  //............ event processing ..........
  for(int ntr=0; ntr< nTrack; ntr++)
    totIter+=oneTrack();
  
  
  if(fpOut  ) { //................ OUTPUT FILE

    if (  allowBigWrite ||  writeTotMB< parHardWriteMax_MB ) {
      int  writeBlock = writeEventSize_kB*1024;
      writeTotMB+=writeEventSize_kB/1024.;
      int j=(random()%sizeN)/3;
      fwrite(bigArrayM[0]+j, 1, writeBlock, fpOut);
      //  size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
    } else {
      printf("write limit exceeded , skip\n");
    }
  }

  if(leakRAM_MB>0) { //................ add memory leak
    int sizeB=leakRAM_MB*1024*1024;
    assert(sizeB>0);
    double *wasteArray= (double*)malloc(sizeB);
    for(int i=0;i<sizeB/16;i++) wasteArray[i]=sin(i);
    printf("  memory leak added %d MB \n",leakRAM_MB);
  }
  clock_t endCpuT = clock();
  double elapsed_cpu_secs = double(endCpuT - beginCpuT) / CLOCKS_PER_SEC;
  time_t endWallT;  time(&endWallT);
  int elapsed_wall_sec = (endWallT - beginWallT); // 1 sec accuracy
  
  printf("%d=id  event: %.2f MOps , elapsed CPU(sec)=%.3g  wallTime=%d(sec), read=%.1f(MB), write=%.1f(MB) \n", eveId, totIter/1e6,elapsed_cpu_secs,elapsed_wall_sec,readTotMB,writeTotMB);
  return elapsed_cpu_secs;
}

void argUsage(char  *argv0 , const char *message=NULL);
//---------------------------------------------------------------------
//---------------------------------------------------------------------
//---------------------------------------------------------------------
int  main(int argc, char *argv[]) {
  char  *userName    = getenv("USER");

  // variables manipulated at exec time
  int useRAM_MB  = 500;
  int virtRAM_MB  = 0;
  int runTime_min = 60;
  int eventTime_sec = 15;
  char  *coreName  = (char *)"vet-ver3"; 
  char  *workPath  = (char *)"/tmp/"; 
  char  *inputName  = (char *)"fixMe1"; 
  int    debugMode  = false;
  int    setNumEvent=-1; // activates fixWork mode
  int  numTrack=20;
  int timeMode=1; // fixTime per event, 0=fixedWork per events

  // working variables for decodding run time params
  int    optInd   = 0;
  char   optChar  = 0;  
  static struct option optLong[] = {
    { "debug"      , 0 , &debugMode     , true},
    { "allowBigWrite", 0 , &allowBigWrite     , true},
    { "help"       , 0 , 0              , 'h' },
    { "useRAM"   , 1 , 0    , 'R' },
    { "virtRAM"   , 1 , 0    , 'V' },
    { "leakRAM"   , 1 , 0    , 'L' },
    { "runTime"   , 1 , 0   , 'T' },
    { "coreName" , 1 , 0 , 'c' },
    { "workPath" , 1 , 0 , 'P' },
    { "writeEventSize" , 1 , 0 , 'w' },
    { "readEventSize" , 1 , 0 , 'r' },
    { "inputName" , 1 , 0 , 'i' },
    { "setEventTime" , 1 , 0 , 't' },
    { "setEventWork" , 1 , 0 , 'W' },
    { "setNumEvent" , 1 , 0 , 'n' },
    { 0, 0, 0, 0}
  };


  // arguments and init
  char  * argv0 = strrchr(argv[0],'/');
  argv0 = ( argv0 == NULL )  ? argv[0] : ++argv0 ;
  
  opterr = 0;
  while((optChar = getopt_long(argc,argv,"hdBR:V:L:T:c:P:w:i:r:t:W:n:",optLong,&optInd)) != EOF) {
    switch(optChar) {
    case  0  :   break;
    case 'R' : useRAM_MB  = atoi(optarg)/MA;     break; 
    case 'V' : virtRAM_MB  = atoi(optarg)/MA;     break; 
    case 'L' : leakRAM_MB  = atoi(optarg);     break; 
    case 'T' : runTime_min  = atoi(optarg);     break; 
    case 'c' : coreName = optarg;    break; 
    case 'P' : workPath = optarg;    break; 
    case 'w' : writeEventSize_kB= 1000*atoi(optarg);     break; 
    case 'i' : inputName = optarg;    break; 
    case 'r' : readEventSize_kB= 1000*atoi(optarg);     break; 
    case 't' : eventTime_sec  = atoi(optarg);     break; 
    case 'n' : setNumEvent  = atoi(optarg);     break; 
    case 'W' : numTrack = atoi(optarg);     break; 
    case 'd' : debugMode= true;         break;
    case 'B' : allowBigWrite= true;         break;
    case 'h' : argUsage(argv0); return(0);         break;
    case '?':   
      if (isprint (optopt))
	fprintf (stderr, "\nUnknown option `-%c'\n\n", optopt);
      else
	fprintf (stderr,"\nUnknown option character `\\x%x'.\n\n",optopt);
    default  : argUsage(argv0,"unknown option");    break;

    };
  }
  
 /* Print any remaining command line arguments (not options).   */
  if (optind < argc)    {
      printf ("\n WARN, non-options ARGV-elements: ");
      while (optind < argc)
	printf ("%s ", argv[optind++]);
      putchar ('\n');
      return -3;
  }

  if (setNumEvent > 0) timeMode=0;
  printf("\n**** Final paramater choice made by user=%s  *** \n",userName);
  printf("Exec:  %s  useRAM/MB=%d  virtRAM/MB=%d coreName='%s' leak=%dMB/eve workPath=%s\n",argv0,useRAM_MB*MA,virtRAM_MB*MA,coreName,leakRAM_MB,workPath);
  printf("  event size: write=%d kB,  read=%d kB, inputName=%s debug=%d allowBigWrite=%d\n",writeEventSize_kB, readEventSize_kB,inputName ,debugMode,allowBigWrite);
  if ( timeMode==1)
    printf("Fixed timeMode selected, runTime/min=%d , eventTime=%d sec\n"
	   ,runTime_min, eventTime_sec);
    else
      printf("Fixed workMode selected, numEvent=%d  work per event =%d (tracks)\n"
	     ,setNumEvent,numTrack);

  srand(123);   // set random generator  only for output file name
  
  if(virtRAM_MB>0) {
    int k=virtRAM_MB, k0=1900;

    if ( k< k0 ) { 
      initRamArray(virtRAM_MB); 
    } else {
      initRamArray(k0);
      initRamArray(k-k0);
    }
  }

  initRamArray(useRAM_MB); // this is unused duplicated ram
  
  //............. prepare INPUT FILE, filename will be used later of re-open is needed
  if(readEventSize_kB>0) {
    printf("open read-file =%s=, strlen=%d verify event time  ...\n",inputName,(int)strlen(inputName));
    assert(readEventSize_kB*1e3  <  useRAM_MB *1e6/2);
    fpInp=fopen(inputName, "rb");
    assert(fpInp);
    oneEvent(66,numTrack); // run it once to see if reading slows the math-ops loop
  }

  //................ prepare OUTPUT FILE
  char fileName[1024];  
  if(writeEventSize_kB>0) {
    sprintf(fileName,"%s/%s_%d.out",workPath,coreName,(int)(rand()%1000000));
    if (strlen(inputName)>8 )  sprintf(fileName,"%s.out",inputName);
    printf("open write-file =%s=, verify event time  ...\n",fileName);
    assert( writeEventSize_kB*1e3  <  useRAM_MB *1e6/2); // increase RAM use
    fpOut=fopen(fileName, "wb");
    assert(fpOut);
    oneEvent(77,numTrack); // run it once to see if writing slows the math-ops loop
  }

  oneEvent(55,numTrack); // discard the 1st measurement beacuse is it is always slower

  printf(" test RAM I/O+ Math CPU speed, use nTrack=%d ... \n", numTrack);
  double sumT=0, nTry=0;
  for(int k=0;k<4;k++) { 
    sumT+=   oneEvent(k,numTrack);
    nTry+=1;
  }
  double avrT=sumT/nTry;
 
  printf("Estimated event processing time (sec) %.2f\n",avrT);

  if ( timeMode==1){
    printf("adjusted  new nTrack=%d\n verify final event length ....\n",numTrack);
    numTrack=(int) ( numTrack * eventTime_sec / avrT); 
    oneEvent(5,numTrack);
  }
    
  if(writeEventSize_kB>0) {
    printf("close/open output file=%s\n",fileName);
    fclose(fpOut);
    writeTotMB=0;
    fpOut=fopen(fileName, "wb");
    assert(fpOut);
  }

  //..................... DO THE WORK .............
  printf("\n***********\n   Start the long event loop ");
 

  if ( timeMode==1){
    printf("the goal is %d minutes, estimate: %.0f events) \n\n",runTime_min,runTime_min*60./eventTime_sec);
  } else {
    double guesT_min=avrT*setNumEvent/60.;
    printf(" the goal is to finish %d events  , estimated time=%.2f minutes\n",setNumEvent, guesT_min);
  }

  time_t beginWallT; time(& beginWallT); // 1 second accuracy
 
  int k=-1;
  double elapsed_wall_min =0;
  while(true) {
    k++;
    oneEvent(k,numTrack); // 1 second accuracy
    time_t endWallT;  time(&endWallT);
    elapsed_wall_min = (endWallT - beginWallT)/60.; // 1 sec accuracy
    
    //printf("  accumulated: minutes wall=%.1f  \n", elapsed_wall_min);

    if(  timeMode==1 && elapsed_wall_min > runTime_min ) { // do not continue
      break;
    }
    if(  timeMode==0 &&  k>=setNumEvent) break;

    if(fpInp==0 && readEventSize_kB>0) {
      printf("    ---- re-open input file=%s\n",inputName);
      fpInp=fopen(inputName, "rb");
      assert(fpInp);
    }
    
  }

  int cpuWork=k*numTrack;

  printf("\n********\n  DONE: %d events, cpuWork=%d, read/MB=%.3g  , wrote/MB=%.3g time= %.2f min of wall clock time\n",k, cpuWork,readTotMB,writeTotMB,elapsed_wall_min);
  if(writeEventSize_kB>0) {
    printf("  closing  output file:  ls -lh %s\n",fileName);
    fclose(fpOut);
   }


  }



//=============================================================
void argUsage(char  *argv0 , const char *message){
  if (message) fprintf(stderr,"%s: %s\n",argv0,message);
  fprintf(stderr,"usage: %s  [OPTIONS]\n",argv0);
  fprintf(stderr," -R | --useRAM (MB) [500]    : size of RES_RAM array, random access\n");
  fprintf(stderr," -V | --virtRAM (MB) [0]    : size of VIRT_RAM-only array, never used , dead wood \n");
  fprintf(stderr," -L | --leakRAM (MB/event) [0]    : size of memory leak per event , will decay\n");
  fprintf(stderr," -c | --coreName [vet-ver3]  : for all outputs from this job\n");
  fprintf(stderr," -P | --workPath [/tmp/]     : for all outputs from this job\n");
  fprintf(stderr," -w | --writeEventSize (MB) [-1] :  default=off \n");
  fprintf(stderr," -B | --allowBigWrite :  default=off \n");
  fprintf(stderr," -i | --inputName [-]            : full input file name , provide if reading\n");
  fprintf(stderr," -r | --readEventSize (MB) [-1]  : file must exist, default=off \n");
  fprintf(stderr," -d | --debug        : set debug mode on\n");
  fprintf(stderr," -h | --help         : this short help\n");
  fprintf(stderr,"Either: (default)\n");
  fprintf(stderr," -t | --setEventTime (sec) [15] : auto-tuned wall clock time per event \n");
  fprintf(stderr," -T | --runTime (min) [60]   : wall-time duration of this job \n");
  fprintf(stderr,"Or: (disables the above)\n");
  fprintf(stderr," -W | --setEventWork [20] (tracks) : fixed amount of computation per event\n");
  fprintf(stderr," -n | --setNumEvent [10] : fixed number of events, (is the switch)\n");

  if(message) exit(-1);
  return;
}

