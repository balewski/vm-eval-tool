#!/bin/bash
# Author:  Jan Balewski, 2015 - 2017
# instruction : https://bitbucket.org/balewski/vm-eval-tool/src/master/JobPump.md
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value

# optimize  jobPump setting
export MAX_JOBS_LOAD=7 
export PEAK_W_LOAD=64
export LOW_FREE_RAM=3  #GB
export EXEC_NAME=vet3.exe
# optional:
#export TOT_JOB_LIMIT=20

# optional, common params used by the task itself
export RUN_TIME=3

echo fire the job pump :
./jobPumpUtil.sh  jobPumpTask.list
