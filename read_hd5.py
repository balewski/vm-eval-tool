#!/usr/bin/env python
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import h5py

#...!...!..................
def write_data_hdf5(dataD,outF,verb=1):
    h5f = h5py.File(outF, 'w')
    if verb>0: print('saving data as hdf5:',outF)
    for item in dataD:
        rec=dataD[item]
        h5f.create_dataset(item, data=rec)
        if verb>0:print('h5-write :',item, rec.shape)
    h5f.close()
    xx=os.path.getsize(outF)/1048576
    print('closed  hdf5:',outF,' size=%.2f MB'%(xx))


#...!...!..................
def read_data_hdf5(inpF):
        print('read data from hdf5:',inpF)
        h5f = h5py.File(inpF, 'r')
        objD={}
        for x in h5f.keys():
            obj=h5f[x][:]
            print('read ',x,obj.shape)
            objD[x]=obj

        h5f.close()
        return objD

#=================================
#=================================
#  M A I N 
#=================================
#=================================

# module load tensorflow/gpu-1.15.0-py37
fname='/global/cscratch1/sd/balewski/cosmoUniverse_2019_05_4parE_peter/multiScale_tryG/PeterA_2019_05_4parE-rec100.h5'
blob=read_data_hdf5(fname)
