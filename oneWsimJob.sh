#!/bin/bash
core=$1
numEve=$2
dataPath=$3
echo  oneWsimJob.sh core=$core   numEve=$numEve  dataPath=$dataPath
cd $dataPath
pwd
starver SL13a
echo $STAR
starsim -w 0 -b ppWprod.kumac $numEve $core  $RANDOM  >& Log-starsim.$core

root4star -q -b bfc.C\($numEve,\"MakeEvent,ITTF,NoSsdIt,NoSvtIt,Idst,VFPPVnoCTB,logger,-EventQA,-dstout,tags,Tree,-EvOut,analysis,dEdxY2,IdTruth,useInTracker,-hitfilt,tpcDB,TpcHitMover,TpxClu,McAna,fzin,y2009,tpcrs,geant,-geantout,beamLine,eemcDb,-McEvOut,bigbig,emcY2,EEfs,bbcSim,ctf,CMuDST\",\"$core.fzd\"\)  >& Log-bfc.$core 

