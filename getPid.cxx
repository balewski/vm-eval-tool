#include <stdio.h>
#include <stdlib.h>

// it is not trivial - this solution searches for all process matching program name

int main(int argc, char* argv[]) {
  if(argc<2) { printf("provide name of the process you want to see PID, abort\n"); exit(1);}
  char psBuffer[128], buff1[1024]; 
  FILE *iopipe;
  sprintf(buff1,"pgrep %s",argv[1]);
  printf("search for proces=%s=\n",argv[1]);

  if( (iopipe = popen( buff1, "r" )) == NULL ) exit( 1 ); //ampl is the command (executable) whose pid you want to find
 while( !feof( iopipe ) ) {
   if( fgets( psBuffer, 128, iopipe ) == NULL ) break;
   int pid=atoi( psBuffer);
   printf( "\nProcess=%s  PID= %d\n",argv[1],pid);
 }

 printf( "\nProcess returned %d\n", pclose( iopipe ) );

 return 0;
}
