 a set of scripts for evaluation of CPU performance

To compile vet-exe do
//  to compile:  c++ vet_v3.cxx -o vet3.exe

To fuly simulating read/write/comput for fixed amount of work execute

time dd if=/dev/urandom of=/tmp/sample1G.dat bs=64M count=16 
real	2m31.401s

./vet3.exe --useRAM 800 --setNumEvent 12 --setEventWork 30 --writeEventSize 3 --readEventSize 10 --inputName /tmp/sample1G.dat  --coreName mockJan

Special macro to evaluate I/O speed

ioSpeed1.sh  targetDir  size/kB  blockSize/kB
>>> writeReal writeUser readReal readUser



===================   Tune to look like Physics job  =============
Fixed amount of work
run 20 min
RAM 1.5GB 
200 events  (~6 sec/event)
read 400 MB  /global/project/projectdirs/mpccc/balewski/pdsfSpeed_inp
write 200 MB /global/project/projectdirs/mpccc/balewski/pdsfSpeed_out

CORE='mockJan2'
INPD='sample1G.dat2'
./vet3.exe --useRAM 1500 --setNumEvent 20 --setEventWork 80 --writeEventSize 1 --readEventSize 2 --workPath /global/project/projectdirs/mpccc/balewski/pdsfSpeed_out --inputName  /global/project/projectdirs/mpccc/balewski/pdsfSpeed_inp/${INPD} --coreName ${CORE}

***********
   Start the long event loop  the goal is to finish 20 events  , estimated time=1.94 minutes
0=id  event: 17.60 MOps , elapsed CPU(sec)=5.95  wallTime=6(sec), read=15.6(MB), write=6.8(MB) 
1=id  event: 17.60 MOps , elapsed CPU(sec)=6.13  wallTime=6(sec), read=17.6(MB), write=7.8(MB) 
2=id  event: 17.60 MOps , elapsed CPU(sec)=6.42  wallTime=6(sec), read=19.5(MB), write=8.8(MB) 
3=id  event: 17.60 MOps , elapsed CPU(sec)=5.9  wallTime=6(sec), read=21.5(MB), write=9.8(MB) 
4=id  event: 17.60 MOps , elapsed CPU(sec)=5.88  wallTime=6(sec), read=23.4(MB), write=10.7(MB) 
5=id  event: 17.60 MOps , elapsed CPU(sec)=6.03  wallTime=6(sec), read=25.4(MB), write=11.7(MB) 
6=id  event: 17.60 MOps , elapsed CPU(sec)=5.89  wallTime=6(sec), read=27.3(MB), write=12.7(MB) 
7=id  event: 17.60 MOps , elapsed CPU(sec)=7.89  wallTime=8(sec), read=29.3(MB), write=13.7(MB) 
8=id  event: 17.60 MOps , elapsed CPU(sec)=7.91  wallTime=8(sec), read=31.2(MB), write=14.6(MB) 
9=id  event: 17.60 MOps , elapsed CPU(sec)=6.8  wallTime=7(sec), read=33.2(MB), write=15.6(MB) 
10=id  event: 17.60 MOps , elapsed CPU(sec)=5.86  wallTime=6(sec), read=35.2(MB), write=16.6(MB) 
11=id  event: 17.60 MOps , elapsed CPU(sec)=5.84  wallTime=5(sec), read=37.1(MB), write=17.6(MB) 
12=id  event: 17.60 MOps , elapsed CPU(sec)=5.87  wallTime=6(sec), read=39.1(MB), write=18.6(MB) 
13=id  event: 17.60 MOps , elapsed CPU(sec)=6.11  wallTime=6(sec), read=41.0(MB), write=19.5(MB) 
14=id  event: 17.60 MOps , elapsed CPU(sec)=8.34  wallTime=9(sec), read=43.0(MB), write=20.5(MB) 
15=id  event: 17.60 MOps , elapsed CPU(sec)=8.03  wallTime=8(sec), read=44.9(MB), write=21.5(MB) 
16=id  event: 17.60 MOps , elapsed CPU(sec)=8.03  wallTime=8(sec), read=46.9(MB), write=22.5(MB) 
17=id  event: 17.60 MOps , elapsed CPU(sec)=7.99  wallTime=8(sec), read=48.8(MB), write=23.4(MB) 
18=id  event: 17.60 MOps , elapsed CPU(sec)=7.91  wallTime=8(sec), read=50.8(MB), write=24.4(MB) 
19=id  event: 17.60 MOps , elapsed CPU(sec)=7.95  wallTime=8(sec), read=52.7(MB), write=25.4(MB) 

********
  DONE: 20 events, cpuWork=1600, read/MB=52.7  , wrote/MB=25.4 time= 0.038  hours of wall clock time

=======================
=======================
=======================

Same job on PDSF w/o reading & writing

./vet3.exe --useRAM 1500 --setNumEvent 20 --setEventWork 80


***********
   Start the long event loop  the goal is to finish 20 events  , estimated time=2.09 minutes
0=id  event: 17.60 MOps , elapsed CPU(sec)=6.24  wallTime=6(sec), read=0.0(MB), write=0.0(MB) 
1=id  event: 17.60 MOps , elapsed CPU(sec)=6.22  wallTime=6(sec), read=0.0(MB), write=0.0(MB) 
2=id  event: 17.60 MOps , elapsed CPU(sec)=6.27  wallTime=7(sec), read=0.0(MB), write=0.0(MB) 
3=id  event: 17.60 MOps , elapsed CPU(sec)=6.32  wallTime=6(sec), read=0.0(MB), write=0.0(MB) 
4=id  event: 17.60 MOps , elapsed CPU(sec)=8.84  wallTime=9(sec), read=0.0(MB), write=0.0(MB) 
5=id  event: 17.60 MOps , elapsed CPU(sec)=9.83  wallTime=10(sec), read=0.0(MB), write=0.0(MB) 
6=id  event: 17.60 MOps , elapsed CPU(sec)=10  wallTime=10(sec), read=0.0(MB), write=0.0(MB) 
7=id  event: 17.60 MOps , elapsed CPU(sec)=10.6  wallTime=10(sec), read=0.0(MB), write=0.0(MB) 
8=id  event: 17.60 MOps , elapsed CPU(sec)=9.85  wallTime=10(sec), read=0.0(MB), write=0.0(MB) 
9=id  event: 17.60 MOps , elapsed CPU(sec)=8.81  wallTime=9(sec), read=0.0(MB), write=0.0(MB) 
10=id  event: 17.60 MOps , elapsed CPU(sec)=8.11  wallTime=8(sec), read=0.0(MB), write=0.0(MB) 
11=id  event: 17.60 MOps , elapsed CPU(sec)=6.2  wallTime=6(sec), read=0.0(MB), write=0.0(MB) 
12=id  event: 17.60 MOps , elapsed CPU(sec)=6.12  wallTime=6(sec), read=0.0(MB), write=0.0(MB) 
13=id  event: 17.60 MOps , elapsed CPU(sec)=6.35  wallTime=7(sec), read=0.0(MB), write=0.0(MB) 
14=id  event: 17.60 MOps , elapsed CPU(sec)=6.25  wallTime=6(sec), read=0.0(MB), write=0.0(MB) 
15=id  event: 17.60 MOps , elapsed CPU(sec)=6.2  wallTime=6(sec), read=0.0(MB), write=0.0(MB) 
16=id  event: 17.60 MOps , elapsed CPU(sec)=7.86  wallTime=8(sec), read=0.0(MB), write=0.0(MB) 
17=id  event: 17.60 MOps , elapsed CPU(sec)=9.94  wallTime=10(sec), read=0.0(MB), write=0.0(MB) 
18=id  event: 17.60 MOps , elapsed CPU(sec)=9.42  wallTime=9(sec), read=0.0(MB), write=0.0(MB) 
19=id  event: 17.60 MOps , elapsed CPU(sec)=6.31  wallTime=7(sec), read=0.0(MB), write=0.0(MB) 

********
  DONE: 20 events, cpuWork=1600, read/MB=0  , wrote/MB=0 time= 0.043  hours of wall clock time
