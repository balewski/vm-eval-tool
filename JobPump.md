# JobPump -  many 1 core tasks embarrassingly parallelized 

Assume you have:

* 1-vCore task which can run for a variable amount of time, 

* access to a 60 vCore machine on which you want to run your task in 60 copies, schedule multiple waves until 24 h passes on the wall clock time.

**JobPump** is the shell script which will drive the desired number of concurent tasks pulled sequentially form  the  user provided task list.


This is an example of a tasks list, each task can take distinct argument(s):
```bash
$ cat jobPumpTask.list
~/vm-eval-tool/vet3.exe  --coreName aa0 -T $RUN_TIME  >& ~/tmp/vet-aa0
~/vm-eval-tool/vet3.exe  --coreName aa1 -T $RUN_TIME   >& ~/tmp/vet-aa1
~/vm-eval-tool/vet3.exe  --coreName aa2 -T $RUN_TIME   >& ~/tmp/vet-aa2
~/vm-eval-tool/vet3.exe  --coreName aa3  -T $RUN_TIME  >& ~/tmp/vet-aa3
...
~/vm-eval-tool/vet3.exe  --coreName aa7777  -T $RUN_TIME  >& ~/tmp/vet-aa7777
```

To optimize the load on the node  user needs to decide:

* how many instances of user task should run concurently (MAX_JOBS_LOAD). Note, there may be other tasks running on the shared node - those do not count.

* Before the new task is being launched  the peak load on machine must be below PEAK_W_LOAD and the free RAM must be above LOW_FREE_RAM. 

* Finally, the name of executable (EXEC_NAME) must be passed to the jobPump regardless of content of the task list. 'pgrep $EXEC_NAME' is used to count rouning jobs. 

* Optionally, jobPump will stop launching new tasks after reaching the job count limit of TOT_JOB_LIMIT (if defined by the user), even if the task list is longer - this is convenient during debugging.

```bash
$ cat runJobPump.sh
#!/bin/sh

# optimize  jobPump setting
export MAX_JOBS_LOAD=7 
export PEAK_W_LOAD=64
export LOW_FREE_RAM=3  #GB
export EXEC_NAME=vet3.exe
# optional:
export TOT_JOB_LIMIT=20

# optional, common params used by the task itself
export RUN_TIME=3

echo fire the job pump :
./jobPumpUtil.sh  jobPumpTask.list
```

Launch the jobPump:
```bash
pdsf6 $ ./runJobPump.sh
fire the job pump :
star-pump-1Node start maxJobLoad=7,peakWLoad=64,lowFreeRam=3
execName=vet3.exe, totJobLimit=999888, running  in ~balewski/vm-eval-tool
starting submitting from taskList=jobPumpTask.list ...
pgrep nRun=0 vs. limit=7
current node w-load=5 vs. limit=64
current freeRam_GB=86 vs. limit=3
  checkA_0 tot nJobs=0 ,  sleep 2 seconds ...Tue Jul 18 15:39:43 PDT 2017
myjob to be fired is =~/vm-eval-tool/vet3.exe --coreName aa0 --runTime $RUN_TIME >& ~/tmp/vet-aa0 &=
w=5 ram=86 nr=0 del=59 now sleep for myDelay=3
pgrep nRun=1 vs. limit=7
current node w-load=5 vs. limit=64
current freeRam_GB=86 vs. limit=3
  checkA_1 tot nJobs=1 ,  sleep 3 seconds ...Tue Jul 18 15:39:46 PDT 2017
myjob to be fired is =~/vm-eval-tool/vet3.exe --coreName aa1 --runTime $RUN_TIME >& ~/tmp/vet-aa1 &=
w=5 ram=86 nr=1 del=59 now sleep for myDelay=3
...
```
On a separate xterm you can monitor the requested number of jobs have started

```bash
 PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                                                                                          
14507 balewski  20   0  513m 501m 1068 R 100.0  0.4   0:52.68 vet3.exe                                            
15483 balewski  20   0  513m 501m 1068 R 100.0  0.4   0:42.38 vet3.exe                                                                                                 
13969 balewski  20   0  513m 501m 1072 R 100.0  0.4   0:58.79 vet3.exe                                                  
14246 balewski  20   0  513m 501m 1072 R 100.0  0.4   0:55.74 vet3.exe                                                  
14868 balewski  20   0  513m 501m 1068 R 100.0  0.4   0:49.63 vet3.exe                                                  
15140 balewski  20   0  513m 501m 1068 R 100.0  0.4   0:45.43 vet3.exe                                                  
16319 balewski  20   0  513m 501m 1068 R 100.0  0.4   0:39.31 vet3.exe                                                  
                                               
```

The source code is avaliable at:

git clone  https://bitbucket.org/balewski/vm-eval-tool


Loose comments:
jobPump was meant to be simple to understand. You can of course copy it and modify to your liking. If you come up with some interesting upgrades I'd like to implement them in my version too. 

For testing you can compile my vet_3.cpp code (instruction in the top of the .cpp). It does a lot of computations and you can control how long it runs by passing the run time as -T parameter (in minutes). To see help run it with -h. 

The whole idea of jobPump is to run many tasks one after another, if 1 task finishes the next one should be started - hence the name of the script - it keeps constant 'pressure of jobs' on the node. 

Every Slurm job must use different task list - this is the limitation. Job pump will not cross the node boundary. 
If you want to run on 4 *different* nodes you must have 4 different tasks lists and 4 different slurm jobs (use job array if large number of nodes is need). 
It is pointless to run 2 jobPumps on the same node , rather run 1 and double the number of concurrent tasks. 

On PDSF, use only -p=shared -n60 to assure only single jobPump per node is launched, or run only one Slurm job with 1 jobPump. 
I was assuming you will test 1 jobPump on PDSF and run at scale on Cori with -q=regular -N1 - which gives you the full node per Slurm job. 

JobPump does not obey any SLURM RAM limitations - so you need to match it manually to Slurm command. 
You can tell Slurm --mem 10GB but tell job pump to use 123GB and jobPum will use 123GB. You need to think it through. 


