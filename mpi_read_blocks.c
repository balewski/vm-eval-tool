#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <mpi.h>

#define BUFSZ 16*1024

int  main(int argc, char *argv[])
{
  MPI_Init(&argc, &argv);
  
  assert (argc==2);
  char *fname=argv[1];
  printf("open file=%s, BS=%d\n",fname,BUFSZ);
  FILE *fd = fopen(fname, "rb");
  assert(fd);
  unsigned char buf[BUFSZ*2] = {0};
  
  int nB=0;
  while ( fread(&buf, 1, BUFSZ, fd)==BUFSZ ) {
    nB++;
  }
  fclose(fd);
  
  float totMB=nB*BUFSZ/1024./1024.;
  printf("done, read %d blocks of size %d, total %.4f MB\n",nB,BUFSZ,totMB);

  MPI_Finalize();
  return 0;
}
