#!/bin/bash
#
# Here is an geenpol example of a basic script that specifies the working directory, the shell and the queue.  The #$ must be used to specify the grid engine options used by qsub.  
#
#$ -v env_var=<value>
## run the job in the current working directory (where qsub is called)
#$ -cwd
## specify an email address
#$ -M uname@lbl.gov
## specify when to send the email when job is (a)borted, (b)egins or (e)nds normally
#$ -m abe
## Specify a run time of 11 hours (or 39600 seconds)
#$ -l h_rt=11:00:00
## Specify 12G of memory for the job
#$ -l ram.c=12G

## Your job info goes here
./vet2.exe -T 2