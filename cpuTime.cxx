//* example how to measure CPU & wall clock time
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctime>
using namespace std;

int main() {
  printf(" wait about 25 seconds at 100CPU ...\n");

  clock_t beginCpuT = clock(); // uSec accuracy
  time_t beginWallT; time(& beginWallT); // 1 second accuracy

   for(int i=0;i<2e8;i++) { // goal: 1ms per internal loop
      double x=random()*1./RAND_MAX;
      double y=random()*1./RAND_MAX;
      double z=sin(x)*pow(y,1.123);
    }// end-of-compute loop
 
   clock_t endCpuT = clock();
   double elapsed_cpu_secs = double(endCpuT - beginCpuT) / CLOCKS_PER_SEC;
   time_t endWallT;  time(&endWallT);
   int elapsed_wall_secs = endWallT - beginWallT;
  printf(" elapsed(sec):  CPU %.1f , wall %d  \n",elapsed_cpu_secs,elapsed_wall_secs);

  return 0;
}
