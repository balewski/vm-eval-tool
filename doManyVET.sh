#!/bin/sh
nJob=7
exec=vet
timeSpace=600
echo used c++ compiler is
c++ --version|head -1
c++ vet_v2.cxx -o vet
if [ $? -ne 0  ]; then echo "C++ compilation failed, abort" ; exit 1; fi
param0="--readFile none --writeFile -1 "
echo  "loop($nJob) for  ./$exec   timeSpace/sec=$timeSpace"

for (( i=1; i <= $nJob; i++ )) ; do
# echo "Random number $i: $RANDOM"
 time=$[1500 + $(expr $i \* 100 ) ]
 ram=$[400 + $(expr $i \* 100 ) ]
 #echo  "fire job $i time/min=$time ram=$ram....."
 params="  --runTime $time  --useRAM $ram $param0 "
 echo  "loop($i of $nJob) for  ./$exec $params "
  (time ./$exec $params ) >& /tmp/vet-jan_log.$i &
  if [ $? -ne 0  ]; then echo "jub submition failed, abort" ; exit 1; fi
  echo "sleep for $timeSpace sec ...."
  sleep $timeSpace
done
sleep 1
echo "active jobs of type $exec "
pgrep $exec |nl
echo " "
echo to kill them all exec:  pkill $exec
echo to clean output exec:   rm -f "/tmp/vet-jan_*"
